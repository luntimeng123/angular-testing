import { AbstractControl } from "@angular/forms";

export function ValidateUsername(control: AbstractControl): { [key: string]: any } | null {

  let Regex_Username = '';
  // if (!control.value.startsWith('s') || !control.value.includes('.hrd')) {
  //   return { invalidUsername: true };
  // }
  // return null;
  return !control.value.startsWith('s') || !control.value.includes('.hrd')
    ? { 'invalidUsername': true } : { 'invalidUsername': false };
}