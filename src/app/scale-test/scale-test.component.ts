import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-scale-test',
  templateUrl: './scale-test.component.html',
  styleUrls: ['./scale-test.component.scss'],
})
export class ScaleTestComponent implements OnInit {
  
  myForm!: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.myForm = this.fb.group({
      username: new FormControl('', [Validators.required,Validators.minLength(5)]),
      phonenumber: this.fb.array([]),
    });
  }

  get getPhoneNumber() {
    return this.myForm.get('phonenumber') as FormArray;
  }

  get getUsername(){
    return this.myForm.get('username')
  }

  addPhone() {
    const address = this.fb.group({
      phone: [],
    });
    this.getPhoneNumber.push(address);
  }

  deletePhone(i: number) {
    return this.getPhoneNumber.removeAt(i);
  }
}
