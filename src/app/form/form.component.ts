import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validator, Validators } from '@angular/forms';

import { ValidateUsername } from '../shared/username.validators';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})

export class FormComponent implements OnInit {

  constructor() { }


  model: any = {
    name:'',
    number:''
  }
  
  onSubmit(){
    console.log("valude",this.model.name,this.model.number);
  }

  ngOnInit(): void {
  }

  profileForm = new FormGroup({
    name: new FormControl('', [ValidateUsername]),
    password: new FormControl('', [Validators.required])
  })

  loginUser() {
    console.log("User", this.profileForm.value.password)
  }

  get name() {
    return this.profileForm.get('name');
  }

  get password() {
    return this.profileForm.get('password');
  }

}
