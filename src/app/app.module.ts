import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { UsernameValidatorDirective } from './shared/username-validator.directive';
import { LoginComponent } from './login/login.component';
import { ScaleTestComponent } from './scale-test/scale-test.component';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    UsernameValidatorDirective,
    LoginComponent,
    ScaleTestComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
