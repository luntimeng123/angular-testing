import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor() { }

  ngOnInit(): void { }
  passwordPattern :string = "^([a-zA-Z0-9]{6,10})$";
  idPattern : string = "^([0-9]{6})$";

  loginForm = new FormGroup({
    id: new FormControl('',
      [
        Validators.required,
        Validators.pattern(this.idPattern)
      ]),
    password: new FormControl('',
      [
        Validators.required,
        Validators.pattern(this.passwordPattern)
      ]),
  });

  onSubmit():void {
    console.log("User",this.loginForm.value)
  }

  get id() {
    return this.loginForm.get('id');
  }

  get password() {
    return this.loginForm.get('password');
  }
}
