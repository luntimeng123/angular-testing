import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';
import { ValidateUsername } from './username.validators';

@Directive({
  selector: '[appUsernameValidator][ngModel]',
  providers: [{provide: NG_VALIDATORS, useExisting: UsernameValidatorDirective, multi: true}]
})
export class UsernameValidatorDirective implements Validator  {

  constructor() { }

  validate(control: AbstractControl): { [key: string]: any } | null { 
    return ValidateUsername (control);  
  }

}
